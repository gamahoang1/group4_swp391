<%-- 
    Document   : PostDetail
    Created on : May 16, 2022, 10:59:43 PM
    Author     : Nguyen Minh Hoang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:08:35 GMT -->
    <head>
        <meta charset="UTF-8">
        <title>Post Detail</title>

        <!-- responsive meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <jsp:include page="/Views/LinkHeader.jsp"/>

    </head>

    <body>
        <jsp:include page ="/Views/Header.jsp"/>     

        <!--Start breadcrumb area-->     
        <section class="breadcrumb-area" style="background-image: url(./images/resources/breadcrumb-bg.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title float-left">
                                <h2>Single Post</h2>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="Homepage.jsp">Home</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                    <li class="active">Post Detail</li>
                                </ul>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End breadcrumb area-->    

        <!--Start blog area-->
        <section id="blog-area" class="blog-single-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-7 col-md-12 col-sm-12">
                        <div class="blog-post">
                            <!--Start Single blog Post-->
                            <div class="single-blog-post single_post">
                                <div class="top-box">
                                    <div class="left post-info-style1">
                                        <h3>06</h3>
                                        <div class="borders"></div>
                                        <p>Dec</p>    
                                    </div>
                                    <div class="right">
                                        <div class="title-holder">
                                            <h3 class="blog-title">Gummy Smile Correction: When Surgery is the only Option.</h3> 
                                        </div>
                                        <div class="meta-box">
                                            <ul class="meta-info">
                                                <li><a href="#"><span class="flaticon-pencil"></span>Lilly Anderson</a></li>
                                                <li><a href="#"><span class="flaticon-document"></span>Technology</a></li>
                                                <li><a href="#"><span class="flaticon-comment"></span>0 Comments</a></li>
                                            </ul>    
                                        </div>
                                    </div>
                                </div>
                                <div class="single-post-image">
                                    <div class="image-box">
                                        <img src="./images/blog/blog-single.jpg" alt="Awesome Image">
                                    </div>
                                </div>
                                <div class="text-box1">
                                    <p>How all this mistaken idea of denouncing pleasure and praising pain was born and we will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful there anyone who loves or pursues or desires to obtain pain of itself.</p>    
                                </div>
                                <div class="main-author-box">
                                    <div class="inner">
                                        <div class="icon-holder">
                                            <img src="./images/icon/quote11.png" alt="Icon">  
                                        </div>
                                        <div class="text">
                                            <p>Complete system there anyone who loves our pursues or desires too obtain pain of itself, because it is pain, work because occasionally all circumstances  give you a completed has any right to find fault with a man.</p>
                                            <h3>Meline Jeans <span>- Dean</span></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-box2">
                                    <h3>How to Avoid Bad Breath?</h3>
                                    <p>Anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur  how all this mistaken idea of denouncing.</p>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <ul>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Brush your teeth after every meal</li>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Clean your teeth every six month in clinic</li>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Use mouthwash or sugar-less gum</li>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Do switch to a new toothbrush every 3/mo</li>
                                            </ul>
                                        </div>
                                        <div class="col-xl-6">
                                            <ul>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Don’t scrub your teeth with toothbrush</li>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Don’t snack on junk food</li>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Don’t smoke if you want to avoid oral cancer</li>
                                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Don’t use your teeth to open packages</li>
                                            </ul>
                                        </div>
                                    </div>    
                                </div>
                                <div class="image-video-gallery-box">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="single-box">
                                                <img src="./images/blog/1.jpg" alt="Awesome Image">
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="video-holder-box2" style="background-image: url(./images/blog/2.jpg);">
                                                <div class="icon-holder">
                                                    <div class="icon">
                                                        <div class="inner">
                                                            <a class="html5lightbox" title="Dento Video Gallery" href="https://www.youtube.com/watch?v=p25gICT63ek">
                                                                <span class="flaticon-multimedia"></span>
                                                            </a>
                                                        </div>   
                                                    </div>
                                                </div>  
                                            </div>    
                                        </div>
                                    </div>    
                                </div>
                                <div class="smile-design-content-box">
                                    <h3>Smile Design or Smile Makeover</h3>
                                    <div class="accordion-box">
                                        <!--Start single accordion box-->
                                        <div class="accordion accordion-block">
                                            <div class="accord-btn"><h4>Tooth color</h4></div>
                                            <div class="accord-content">
                                                <p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it but who has any right  find fault with man who chooses to enjoy a pleasure that has no annoying consequences again is there anyone who loves.</p>
                                            </div>
                                        </div>
                                        <!--End single accordion box-->
                                        <!--Start single accordion box-->
                                        <div class="accordion accordion-block">
                                            <div class="accord-btn active"><h4>Length and width</h4></div>
                                            <div class="accord-content collapsed">
                                                <p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it but who has any right  find fault with man who chooses to enjoy a pleasure that has no annoying consequences again is there anyone who loves.</p>
                                            </div>
                                        </div>
                                        <!--End single accordion box-->
                                        <!--Start single accordion box-->
                                        <div class="accordion accordion-block">
                                            <div class="accord-btn"><h4>Tooth display</h4></div>
                                            <div class="accord-content">
                                                <p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it but who has any right  find fault with man who chooses to enjoy a pleasure that has no annoying consequences again is there anyone who loves.</p>
                                            </div>
                                        </div>
                                        <!--End single accordion box-->
                                    </div>    
                                </div> 
                                <div class="tag-holder-box">
                                    <div class="left float-left">
                                        <h5>Tags:</h5> 
                                        <ul>
                                            <li><a href="#">Regular Clean</a></li>
                                            <li><a href="#">Surgery</a></li>
                                            <li><a href="#">Cosmetic</a></li>
                                        </ul>   
                                    </div>
                                    <div class="right float-right">
                                        <p><i class="fa fa-heart-o" aria-hidden="true"></i>2 Likes</p>    
                                    </div>
                                </div>
                                <div class="author-box-holder">
                                    <div class="inner-box">
                                        <div class="img-box">
                                            <img src="./images/blog/author.png" alt="Awesome Image">
                                        </div>
                                        <div class="text">
                                            <h3>About Lilly Anderson</h3>
                                            <p>Denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                                            <div class="author-social-links">
                                                <ul class="sociallinks-style-one">
                                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="blog-prev-next-option">
                                    <div class="single prev">
                                        <div class="image-thumb">
                                            <img src="./images/blog/next-thumb-1.jpg" alt="Image">
                                        </div>
                                        <div class="title">
                                            <h3>What can be done for gummy smile?</h3>
                                            <a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Previous Post</a>
                                        </div>
                                    </div>
                                    <div class="single next">
                                        <div class="image-thumb">
                                            <img src="./images/blog/prev-thumb-1.jpg" alt="Image">
                                        </div>
                                        <div class="title">
                                            <h3>What can be done for gummy smile?</h3>
                                            <a href="#">Next Post<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>     
                                </div>
                                <div class="inner-comment-box">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="single-blog-title-box">
                                                <h2>Read Comments</h2>
                                            </div>
                                            <!--Start single comment box-->
                                            <div class="single-comment-box">
                                                <div class="img-holder">
                                                    <img src="./images/blog/comment-1.jpg" alt="Awesome Image">
                                                </div>
                                                <div class="text-holder">
                                                    <div class="top">
                                                        <div class="date">
                                                            <h5>Steven Rich – Sep 17, 2018:</h5>
                                                        </div>
                                                    </div>
                                                    <div class="text">
                                                        <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>
                                                    </div>
                                                    <div class="reply-button">
                                                        <a href="#"><span class="icon-reload"></span>Reply</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--End single comment box-->
                                            <!--Start single comment box-->
                                            <div class="single-comment-box mar-left">
                                                <div class="img-holder">
                                                    <img src="./images/blog/comment-2.jpg" alt="Awesome Image">
                                                </div>
                                                <div class="text-holder">
                                                    <div class="top">
                                                        <div class="date">
                                                            <h5>William Cobus – Sep 17, 2018:</h5>
                                                        </div>
                                                    </div>
                                                    <div class="text">
                                                        <p>Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                                    </div>
                                                    <div class="reply-button">
                                                        <a href="#"><span class="icon-reload"></span>Reply</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--End single comment box-->

                                            <!--Start single comment box-->
                                            <div class="single-comment-box mar-left">
                                                <div class="img-holder">
                                                    <img src="./images/blog/comment-3.jpg" alt="Awesome Image">
                                                </div>
                                                <div class="text-holder">
                                                    <div class="top">
                                                        <div class="date">
                                                            <h5>Mark Levis – Sep 18, 2018:</h5>
                                                        </div>
                                                    </div>
                                                    <div class="text">
                                                        <p>Beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernaturaut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequinesciunt neque porro.</p>
                                                    </div>
                                                    <div class="reply-button">
                                                        <a href="#"><span class="icon-reload"></span>Reply</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--End single comment box-->
                                            <!--Start single comment box-->
                                            <div class="single-comment-box">
                                                <div class="img-holder">
                                                    <img src="./images/blog/comment-4.jpg" alt="Awesome Image">
                                                </div>
                                                <div class="text-holder">
                                                    <div class="top">
                                                        <div class="date">
                                                            <h5>Van Wimbilton – Aug21, 2018:</h5>
                                                        </div>
                                                    </div>
                                                    <div class="text">
                                                        <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi.</p>
                                                    </div>
                                                    <div class="reply-button">
                                                        <a href="#"><span class="icon-reload"></span>Reply</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--End single comment box-->
                                        </div>
                                    </div>
                                </div>
                                <div class="add-comment-box">
                                    <div class="single-blog-title-box">
                                        <h2>Leave Your Reply</h2>
                                    </div>
                                    <form id="add-comment-form" action="#">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" name="fname" value="" placeholder="Name*" required="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="email" name="form_email" value="" placeholder="Email Address*" required="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" name="website" value="" placeholder="Website">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <textarea name="comment" placeholder="Comments*" required=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button class="btn-one" type="submit">Submit Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End Single blog Post-->

                        </div>
                    </div>

                    <!--Start sidebar Wrapper-->
                    <div class="col-xl-4 col-lg-5 col-md-7 col-sm-12">
                        <div class="sidebar-wrapper">
                            <!--Start single sidebar-->
                            <div class="single-sidebar">
                                <form class="search-form" action="#">
                                    <input placeholder="Search" type="text">
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                            <!--End single sidebar-->
                            <!--Start single sidebar-->
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Categories</h3>
                                </div>
                                <ul class="categories clearfix">
                                    <li><a href="#">Healthy Teeth <span>(4)</span></a></li>
                                    <li><a href="#">Technology <span>(2)</span></a></li>
                                    <li><a href="#">Dental Care <span>(2)</span></a></li>
                                    <li><a href="#">General Dentistry <span>(5)</span></a></li>
                                    <li><a href="#">Cosmetic Dentistry <span>(4)</span></a></li>
                                    <li><a href="#">Uncategorized <span>(1)</span></a></li>
                                </ul>
                            </div>
                            <!--End single sidebar-->
                            <!--Start single sidebar--> 
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Recent Post</h3>
                                </div>
                                <ul class="recent-post">
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/recent-post-1.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="title-holder">
                                            <p><span class="icon-date"></span>14 Nov, 2018</p>
                                            <h5 class="post-title"><a href="#">Implant care: Single tooth <br>replacement.</a></h5>
                                            <a class="readmore" href="#">Continue Reading.</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/recent-post-2.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="title-holder">
                                            <p><span class="icon-date"></span>22 Oct, 2018</p>
                                            <h5 class="post-title"><a href="#">We are amongest most <br>qualified dentists.</a></h5>
                                            <a class="readmore" href="#">Continue Reading.</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/recent-post-3.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="title-holder">
                                            <p><span class="icon-date"></span>05 Oct, 2018</p>
                                            <h5 class="post-title"><a href="#">If you need a crown or an <br>implant you will pay.</a></h5>
                                            <a class="readmore" href="#">Continue Reading.</a>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                            <!--End single sidebar-->
                            <!--Start single-sidebar-->    
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Instagram</h3>
                                </div>
                                <ul class="instagram">
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-1.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-2.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-3.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-4.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-5.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-6.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                </ul>
                                <div class="follow-us-instagram">
                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i>Follow Us On Instagram</a>
                                </div>
                            </div>
                            <!--End single-sidebar-->
                            <!--Start single-sidebar-->
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Archives</h3>
                                </div>
                                <ul class="archives">
                                    <li><a href="#">August 2018 <span>(4)</span></a></li>
                                    <li><a href="#">July 2018 <span>(2)</span></a></li>
                                    <li><a href="#">March 2018 <span>(2)</span></a></li>
                                    <li><a href="#">February 2018 <span>(3)</span></a></li>
                                    <li><a href="#">December 2017 <span>(1)</span></a></li>
                                </ul>
                            </div> 
                            <!--End single-sidebar--> 
                            <!--Start single-sidebar-->
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Tags</h3>
                                </div>
                                <ul class="popular-tag">
                                    <li><a href="#">Checkup</a></li>
                                    <li><a href="#">Doctor</a></li>
                                    <li><a href="#">Cosmetic</a></li>
                                    <li><a href="#">Healthy Teeth</a></li>
                                    <li><a href="#">Implants</a></li>
                                    <li><a href="#">Oral Surgery</a></li>
                                    <li><a href="#">Plan & Procedures</a></li>
                                    <li><a href="#">Surgery</a></li>
                                    <li><a href="#">Teeth Filling</a></li>
                                    <li><a href="#">Tips</a></li>
                                    <li><a href="#">Teeth Whitening</a></li>
                                    <li><a href="#">Theraphy</a></li>
                                    <li><a href="#">Zygoma</a></li>
                                    <li><a href="#">3D Procedures</a></li>
                                </ul>      
                            </div> 
                            <!--End single-sidebar-->
                        </div>    
                    </div>
                    <!--End Sidebar Wrapper-->

                </div>
            </div>
        </section> 
        <!--End blog area--> 


        <jsp:include page="/Views/Footer.jsp"/>
        <!-- /.End Of Color Palate -->
        <jsp:include page="/Views/LinkFooter.jsp"/>


    </body>

    <!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:08:52 GMT -->
</html>
