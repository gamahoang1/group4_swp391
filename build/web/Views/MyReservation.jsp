<%-- 
    Document   : CustomerDetail
    Created on : May 25, 2022, 3:34:27 PM
    Author     : Nguyen Minh Hoang
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:07:28 GMT -->
    <head>
        <meta charset="UTF-8">
        <title>Blog</title>

        <!-- responsive meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <jsp:include page="/Views/LinkHeader.jsp"/>
        <jsp:useBean id="da" class="dao.PostDao" scope="request"></jsp:useBean>
            <link href="./css/MyReservation.css" rel="stylesheet" type="text/css"/>
        </head>

        <body>
        <jsp:include page ="/Views/Header.jsp"/>     

        <!--Start breadcrumb area-->     
        <section class="breadcrumb-area " style="background-image: url(./images/resources/breadcrumb-bg.jpg); margin-bottom: 30px;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title float-left">
                                <h2>My Reservation</h2>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="Homepage.jsp">Home</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                    <li class="active">My Reservation</li>
                                </ul>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-white mb-5">
                        <div class="card-heading clearfix border-bottom mb-4">
                            <h4 class="card-title">Booking Requests</h4>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled">
                                <li class="position-relative booking">
                                    <div class="media">
                                        <div class="msg-img">
                                            <img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt="">
                                        </div>
                                        <div class="media-body">
                                            <h5 class="mb-4">Sunny Apartment <span class="badge badge-primary mx-3">Pending</span><span class="badge badge-danger">Unpaid</span></h5>
                                            <div class="mb-3">
                                                <span class="mr-2 d-block d-sm-inline-block mb-2 mb-sm-0">Booking Date:</span>
                                                <span class="bg-light-blue">02.03.2020 - 04.03.2020</span>
                                            </div>
                                            <div class="mb-3">
                                                <span class="mr-2 d-block d-sm-inline-block mb-2 mb-sm-0">Booking Details:</span>
                                                <span class="bg-light-blue">2 Adults</span>
                                            </div>
                                            <div class="mb-3">
                                                <span class="mr-2 d-block d-sm-inline-block mb-2 mb-sm-0">Price:</span>
                                                <span class="bg-light-blue">$147</span>
                                            </div>
                                            <div class="mb-5">
                                                <span class="mr-2 d-block d-sm-inline-block mb-1 mb-sm-0">Clients:</span>
                                                <span class="border-right pr-2 mr-2">John Inoue</span>
                                                <span class="border-right pr-2 mr-2"> john@example.com</span>
                                                <span>123-563-789</span>
                                            </div>
                                            <a href="#" class="btn-gray">Send Message</a>
                                        </div>
                                    </div>
                                    <div class="buttons-to-right">
                                        <a href="#" class="btn-gray mr-2"><i class="far fa-times-circle mr-2"></i> Reject</a>
                                        <a href="#" class="btn-gray"><i class="far fa-check-circle mr-2"></i> Approve</a>
                                    </div>
                                </li>

                                <li class="position-relative booking">
                                    <div class="media">
                                        <div class="msg-img">
                                            <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">
                                        </div>
                                        <div class="media-body">
                                            <h5 class="mb-4">Burger House <span class="badge badge-success ml-3">Approved</span></h5>
                                            <div class="mb-3">
                                                <span class="mr-2 d-block d-sm-inline-block mb-2 mb-sm-0">Booking Date:</span>
                                                <span class="bg-light-green">06.03.2020 - 07.03.2020</span>
                                            </div>
                                            <div class="mb-3">
                                                <span class="mr-2 d-block d-sm-inline-block mb-2 mb-sm-0">Booking Details:</span>
                                                <span class="bg-light-green">2 Adults, 2 Children</span>
                                            </div>

                                            <div class="mb-5">
                                                <span class="mr-2 d-block d-sm-inline-block mb-1 mb-sm-0">Clients:</span>
                                                <span class="border-right pr-2 mr-2">Jaime Cressey</span>
                                                <span class="border-right pr-2 mr-2"> jaime@example.com</span>
                                                <span>355-456-789</span>
                                            </div>
                                            <a href="#" class="btn-gray">Send Message</a>
                                        </div>
                                    </div>
                                    <div class="buttons-to-right">
                                        <a href="#" class="btn-gray mr-2"><i class="far fa-times-circle mr-2"></i>Cancled</a>
                                    </div>
                                </li>

                                <li class="position-relative booking">
                                    <div class="media">
                                        <div class="msg-img">
                                            <img src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="">
                                        </div>
                                        <div class="media-body">
                                            <h5 class="mb-4">Modern Hotel <span class="badge badge-danger ml-3">Cancled</span></h5>
                                            <div class="mb-3">
                                                <span class="mr-2 d-block d-sm-inline-block mb-2 mb-sm-0">Booking Date:</span>
                                                <span class="btn-gray">20.03.2020 - 24.03.2020</span>
                                            </div>
                                            <div class="mb-3">
                                                <span class="mr-2 d-block d-sm-inline-block mb-2 mb-sm-0">Booking Details:</span>
                                                <span class="btn-gray">2 Adults</span>
                                            </div>
                                            <div>
                                                <span class="mr-2 d-block d-sm-inline-block mb-1 mb-sm-0">Clients:</span>
                                                <span class="border-right pr-2 mr-2">Tesha Stovall</span>
                                                <span class="border-right pr-2 mr-2"> tesha@example.com</span>
                                                <span>123-456-684</span>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>

                        </div>

                    </div>

                </div>
                <div class="col-md-4">
                    <div class="sidebar-wrapper">
                        <!--Start single sidebar-->
                        <div class="single-sidebar">
                            <form class="search-form" action="#">
                                <input placeholder="Search" type="text">
                                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                        </div>
                        <!--End single sidebar-->
                        <!--Start single sidebar-->
                        <div class="single-sidebar">
                            <div class="sidebar-title">
                                <h3>Categories</h3>
                            </div>
                            <ul class="categories clearfix">
                                <li><a href="#">Healthy Teeth <span>(4)</span></a></li>
                                <li><a href="#">Technology <span>(2)</span></a></li>
                                <li><a href="#">Dental Care <span>(2)</span></a></li>
                                <li><a href="#">General Dentistry <span>(5)</span></a></li>
                                <li><a href="#">Cosmetic Dentistry <span>(4)</span></a></li>
                                <li><a href="#">Uncategorized <span>(1)</span></a></li>
                            </ul>
                        </div>


                        <!--End single-sidebar-->
                        <!--Start single-sidebar-->
                        <div class="single-sidebar">
                            <div class="sidebar-title">
                                <h3>Archives</h3>
                            </div>
                            <ul class="archives">
                                <li><a href="#">August 2018 <span>(4)</span></a></li>
                                <li><a href="#">July 2018 <span>(2)</span></a></li>
                                <li><a href="#">March 2018 <span>(2)</span></a></li>
                                <li><a href="#">February 2018 <span>(3)</span></a></li>
                                <li><a href="#">December 2017 <span>(1)</span></a></li>
                            </ul>
                        </div> 

                    </div>    
                </div>
                <div class="col-md-12" style="margin-bottom: 30px;">
                        <ul class="post-pagination text-center">
                            <li class="float-left"><a class="left" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                            <li class="text-center">1 of 4</li>
                            <li class="float-right"><a class="right" href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div> 
            </div>

        </div>
        <jsp:include page="/Views/Footer.jsp"/>
        <!-- /.End Of Color Palate -->
        <jsp:include page="/Views/LinkFooter.jsp"/>

    </body>
</html>
