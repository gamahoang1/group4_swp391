<%-- 
    Document   : Posts
    Created on : May 16, 2022, 10:56:02 PM
    Author     : Nguyen Minh Hoang
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:07:28 GMT -->
<head>
	<meta charset="UTF-8">
	<title>Blog</title>

	<!-- responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <jsp:include page="/Views/LinkHeader.jsp"/>
    <jsp:useBean id="da" class="dao.PostDao" scope="request"></jsp:useBean>
</head>

<body>
<jsp:include page ="/Views/Header.jsp"/>     

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(./images/resources/breadcrumb-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title float-left">
                       <h2>Post</h2>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="Homepage.jsp">Home</a></li>
                            <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                            <li class="active">Post</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->    

<!--Start blog area-->
<section id="blog-area" class="blog-default-area">
    <div class="container">
        <div class="row">
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post">
                    <div class="img-holder">
                        <img src="./images/blog/v1-1.jpg" alt="Awesome Image">
                        <div class="categorie-button">
                            <a class="btn-one" href="#">Healthy Teeth</a>    
                        </div>
                    </div>
                    <div class="text-holder">
                        <div class="meta-box">
                            <div class="author-thumb">
                                <img src="./images/blog/author-1.png" alt="Image">
                            </div>
                            <ul class="meta-info">
                                <li><a href="#">By Megan Clarks</a></li>
                                <li><a href="#">Nov 14, 2018</a></li>
                            </ul>    
                        </div>
                        <h3 class="blog-title"><a href="PostDetail">What you need to know teeth?</a></h3> 
                        <div class="text-box">
                            <p>It not only helps you to chew and eat your food frames your faceany missing tooth can major impact your quality of life.</p>
                        </div>
                        <div class="readmore-button">
                            <a class="btn-two" href="#"><span class="flaticon-next"></span>Continue REading</a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            <!--Start single blog post-->
            <c:forEach var="x" items="${da.posts}">
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post">
                    <div class="img-holder">
                        <img src="./images/blog/v1-2.jpg" alt="Awesome Image">
                        <div class="categorie-button">
                            <a class="btn-one" href="#">${x.Category}</a>    
                        </div>
                    </div>
                    <div class="text-holder">
                        <div class="meta-box">
                            <div class="author-thumb">
                                <img src="./images/blog/author-2.png" alt="Image">
                            </div>
                            <ul class="meta-info">
                                <li><a href="#">${x.Author}</a></li>
                                <li><a href="#">${x.UpdateDate}</a></li>
                            </ul>    
                        </div>
                        <h3 class="blog-title"><a href="blog-single.html">${x.Title}</a></h3> 
                        <div class="text-box">
                            <p>${x.Description}</p>
                        </div>
                        <div class="readmore-button">
                            <a class="btn-two" href="#"><span class="flaticon-next"></span>Continue Reading</a>
                        </div>  
                    </div>
                </div>
            </div>
            </c:forEach>
            
            <!--End single blog post-->
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="post-pagination text-center">
                    <li class="float-left"><a class="left" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                    <li class="text-center">1 of 4</li>
                    <li class="float-right"><a class="right" href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End blog area-->

<jsp:include page="/Views/Footer.jsp"/>
<!-- /.End Of Color Palate -->
<jsp:include page="/Views/LinkFooter.jsp"/>


</body>

<!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:07:51 GMT -->
</html>
