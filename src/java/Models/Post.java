/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Nguyen Minh Hoang
 */
public class Post {
    private String title;
    private String conttent;
    private String description;
    private String UpdateDate;
    private String thumbnailLink;
    private String Author;
    private String category;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getConttent() {
        return conttent;
    }

    public void setConttent(String conttent) {
        this.conttent = conttent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String UpdateDate) {
        this.UpdateDate = UpdateDate;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String Author) {
        this.Author = Author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Post() {
    }

    public Post(String title, String conttent, String description, String UpdateDate, String thumbnailLink, String Author, String category) {
        this.title = title;
        this.conttent = conttent;
        this.description = description;
        this.UpdateDate = UpdateDate;
        this.thumbnailLink = thumbnailLink;
        this.Author = Author;
        this.category = category;
    }
    
    public String getDatePublic() throws ParseException {
        if(UpdateDate.length()<=7){
            return UpdateDate;
        }
        String s = new SimpleDateFormat("dd-MMM-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(UpdateDate));
        StringBuilder str = new StringBuilder(s.substring(0,6));
        str = str.deleteCharAt(2);
        str = str.insert(2," ");
        return str.toString()   ;
    }


    @Override
    public String toString() {
        return "Post{" + "title=" + title + ", conttent=" + conttent +  ", UpdateDate=" + UpdateDate+ ", thumbnailLink=" + thumbnailLink + ", Author=" + Author + ", category=" + category + '}';
    }

   

    
}
