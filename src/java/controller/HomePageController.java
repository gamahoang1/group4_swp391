/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.CategoryPost;
import dao.ServiceDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Models.CategoryService;
import Models.Post;
import dao.PostDao;

/**
 *
 * @author Nguyen Minh Hoang
 */
public class HomePageController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PostDao pd = new PostDao();
      List<CategoryPost> listCategoryPost = new ArrayList<CategoryPost>();
        listCategoryPost = pd.getCategoryPost();
        List<Post> listPost = new ArrayList<Post>();
        listPost = pd.getPosts();
       request.setAttribute("listPost", listPost);
        request.setAttribute("listCategoryPost", listCategoryPost);
         RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Views/Homepage.jsp");
        dispatcher.forward(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

}
