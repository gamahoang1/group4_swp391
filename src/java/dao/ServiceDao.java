/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import Models.CategoryService;


/**
 *
 * @author Nguyen Minh Hoang
 */
public class ServiceDao {

    DBContext bContext;
    public ServiceDao() {
        this.bContext = new DBContext();
    }
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

 
      public List<CategoryService> getCategoryService() {
          List<CategoryService> list= new ArrayList<CategoryService>();
        try {
           
            String querry = "SELECT * FROM dbo.CategoryService";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            while (rs.next()) {
               CategoryService cs = new CategoryService();
              cs.setId(rs.getInt("ID"));
              cs.setName(rs.getString("CategoryName"));
              cs.setThumbnail(rs.getString("thumbnail"));
              cs.setShortDescription(rs.getString("ShortDescription"));
               list.add(cs);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return list;
    }
      
}

