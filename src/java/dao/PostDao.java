/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import Models.CategoryPost;
import Models.Post;


/**
 *
 * @author Nguyen Minh Hoang
 */
public class PostDao {

    DBContext bContext;
    public PostDao() {
        this.bContext = new DBContext();
    }
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

 
      public List<Post> getPosts() {
          List<Post> listPost= new ArrayList<Post>();
        try {
           
            String querry = "SELECT TOP 3 cp.PostName,p.Title,p.Content,p.[Description],p.UpdateDate,p.ThumbnailLink,p.Author FROM dbo.Posts AS p INNER JOIN dbo.CategoryPost AS cp ON cp.ID = p.CategoryId ORDER BY p.UpdateDate";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            while (rs.next()) {
               Post p = new Post();
               p.setAuthor(rs.getString("Author"));
               p.setCategory(rs.getString("PostName"));
               p.setConttent(rs.getString("Content"));
               p.setDescription(rs.getString("Description"));
               p.setThumbnailLink(rs.getString("ThumbnailLink"));
               p.setTitle(rs.getString("Title"));
               p.setUpdateDate(rs.getString("UpdateDate"));
               listPost.add(p);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return listPost;
    }
      public List<CategoryPost> getCategoryPost() {
          List<CategoryPost> listCategoryPost= new ArrayList<CategoryPost>();
        try {
           
            String querry = "SELECT * FROM dbo.CategoryPost";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            while (rs.next()) {
               CategoryPost p = new CategoryPost();
              p.setId(rs.getInt("ID"));
              p.setName(rs.getString("PostName"));
               listCategoryPost.add(p);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return listCategoryPost;
    }
      
}
